Ploterdino
==========

Plotter realizado a partir de una impresora vieja de inyección de tinta.

Donde veo videos:
-----------------
https://www.youtube.com/watch?v=ge4cFOpa1i0 (Quitar el sonido!!!)


Que se necesita:
----------------
Un Arduino. El proyecto se desarrolla con un Arduino Uno
Una impresora vieja con dos motores de corriente continua

Un Servo de los utilizados en aeromodelismo
// Se utilizará el panel de control de la impresora ya que va separado
Se desarrollará un shield para arduino que lo controle
Un shield "proto" de Arduino. Podremos poner cosas.

Si no tienes el panel puedes hacer uno con:
? pulsadores para direccion del cabezal y para ponerla On-Line como las impresoras
2 Diodos led (o 3)...

.....

Que hay en el repositorio
-------------------------

Este documento

documentacion
   - sketch de manejo del 74hc595 (utilizado en el panel de la impresora y driver motores) y datasheet
   -                  del 74hc165 (utilizado en el panel de la impresora y leer sensores) y datasheet
   - sketch de manejo del l293d que manejará los motores de continua y datasheet
   - sketch de manejo del sensor de posicion de la hp (sensor3050) el otro va igual e imagen con datos
   - sketch de manejo de los leds del panel de la hp
   - JPG con el esquema del shield controlador de Adafruit de motores paso a paso
   - Datasheet del CD4030 puertas XOR (solo version uno)
      

Las librerias necesarias para que anden los motores a través del shield. Carpeta AFmotor. Hay que copiarla en:
/usr/share/arduino/libraries


Información en la web
---------------------
http://arduino.cc/es/Tutorial/HomePage
http://reprap.org/wiki/Optical_encoders_01


TODO
----

Como va a ir la electronica montada
  - los sensores van pegados y llegan al shield. (A un XOR- no, ahora va a un hc165) . En este van las resistencias.
  - Subir y bajar el pen va en el carro y llega al shield (Necesitará poner un transistor??? sustituir por servo)
     (Ver como trabajan los servos y transistores que lleva) Poner un servo directamente
  - Los motores van al shield, que lleva un 595 que manda al l293d. Se puede usar el driver de adafruit 
  - El panel de control va al Shield (2 botones y 2 leds). Se utiliza el reloj de comun y los latch.
//  - Hacer un esquema teorico del panel de la HP. No lo utilizaaré al final
  - Ver si el motor funciona fuera del bucle, es decir si el motor funciona solo cuando se activa la patilla. (En principio debe ser así)
  
El shield alimenta al Arduino. Ver voltaje de los motores. No hace falta que vayan muy rápidos.
Aclarar funcionamiento fisico de los motores.
  - a que "velocidad" empieza a moverse el motor
  - a la minima velocidad. ¿Puede contar? ¿Que longitud tiene el carro? ¿Puede pararse? 
  - Distancia minima y máxima a la que se para con precisión
  

Aclarar la velocidad y montar todo el programa que imprime
Programa "server" que envia datos. Se puede hacer en Python como ejercicio al curso
  
  Patillaje del Arduino
  ---------------------
  - 0  recibe comunicación serial con el ordenador
  - 1  envia comunicación serial con ordenador
  
  Utiliza los pines que utiliza adafruit se podrá utilizar el driver!!!
  - 2  NADA -||| sensor- 1a .XOR. 1b
  - 3  Servo ||| - sensor- 2a .XOR. 2b 
  - 4  Reloj
  - 5  Velocidad motor 2
  - 6  Velocidad motor 1
  - 7  Activa 595 de motores                      || Dir_En
  
  - 8  Serial out                                 || Dir_Ser
  - 9  Serial in                                  || Subir bajar pen (puede conectarse servo)
  - 10 Latch del teclado                          ||sensor- 1a .XOR. 1b (hay que confirmar funcionamiento del 165)
  - 11 NADA                                       || Latch del panel (desactivado no atiende) 
  - 12 Latch del motor                            || Dir_Latch
  - 13 led de la placa (mejor no usar)- desactivado no manda datos (ver funcionamiento panel)


  74HC595 del motor
  -----------------
  
   5- PQ6 - motor 2a (4a en adafruit)
   6- PQ7 - motor 1b (3b en adafruit)
   7- PQ8 - motor 2b (4b en adafruit)
   8- GND
  11- reloj
  12- latch motor
  13- habilita el chip del motor (OE)
  14- serial 
  15- PQ1 - motor 1a (3a en placa adafruit)
  16- 5V
  
  LM293 al motor
  --------------
  1- Velocidad motor 1 - (6 del Arduino)
  2- motor1 adelante
  3- Motor1 +


  4- GND
  5- GND
  6- Motor1 -
  7- motor1 atrás
  8- VCC2 - Corriente de motores ()
  9- Velocidad motor 2 - (5 del Arduino)
  10 - motor2 adelante
  11 - Motor2 +
  12 - GND
  13 - GND
  14 - Motor2 -
  15 - motor2 atrás
  16 - VCC1 - Corriente del chip y la lógica
  
  CD4030 Puertas XOR
  ------------------
  (Ya no se utiliza) 
  
  74hc165 Lee sensores y botones
  ------------------------------  
poner patillaje

  
  
Patillaje del panel de la hp (Lado de la baquelita, conector a la derecha)(no la utilizo)
----------------------------
1- Serial - OUT
  2- 5V
3- Reloj
  4- GND
5- Serial - IN
  6- Latch

Leds del panel de la HP (no lo utilizo)
-----------------------
Se envian el MSBF (el mas significante primero) 
El primer octeto es el digito y se apaga el numero enviado
0- Se enciende todos
2- arriba derecha
4- centro
8- abajo derecha
16 abajo
32 abajo izquierda
64 arriba izquierda
128 arriba
256 se apagan todos
no se encienden los puntos

El segundo octeto

0- 9 apagado 8 encendido (color/negro)
2- se encienden todos
4- 2 apagado
8- 4 apagado
16 6 apagado
32 1 apagado
64 5 apagado
128 7 apagado
256 3 apagado
(esto no está muy claro pero...)

Secuencia del encoder
---------------------

Es la esperada 00, 01, 11, 10 
Ponemos un XOR y así se alternan 1 y 0 en cada paso y solo atendemos una patilla que se podrá ir contando

Funcionamiento:
---------------
    Al encender.- a)está en 0 puede que esté en trozo blanco. Se mueve en dirección lenta hacia adelante midiendo el tiempo. Si tarda mucho, cuando ve negro se para, ya está al principio. b)Si tarda poco o es negro desde principio es porque está en mitad de la impresora. Dar marcha atrás hasta que esté en blanco el tiempo de 5 rayas. Ir al caso 1.
Habrá que medir el numero de rayas que hay en el carro. O bien que las mida el cada vez que se encienda.


	En la nueva versión con el lector paralelo. Cuando termina de hacer esto... comprueba que no hay papel pita el motor y parpadea el led (que es nuevo en el 595, aun no está puesto). Cuando nota el papel por sensor de detrás, enciende el led de online, se pone el carro en movimiento hasta que el sensor de alante se active. Empieza a contar pasos hasta que el sensor de atrás se active y memoriza los pasos. Vuelve a la posición inicial, es decir justo cuando no detecta papel delante. Y pita de nuevo. Cuando hace esto debe medir el papel, para saber de que tamaño es.

	Dispondrá de dos botones uno de online y otro de paper feed 1 linea.

Mientras que esté online pinta. Comprobando que los puntos estan dentro de los límites
