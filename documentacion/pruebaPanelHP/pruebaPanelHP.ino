/* ************************************************************
*  Naranja +5
*  Marron Gnd
*  Azul 8
*  B/Azul 11
*  B/Marron 12
************************************************************* */

//Pin connected to ST_CP of 74HC595
int latchPin = 8;
//Pin connected to SH_CP of 74HC595
int clockPin = 12;
////Pin connected to DS of 74HC595
int dataPin = 11;
////Pin conectado al led
int led = 13;
byte a= B00000001;
byte b=B11111111;
int c;
//byte b;
 

void setup() {
  //set pins to output so you can control the shift register
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  pinMode(led,OUTPUT);
  digitalWrite(led,HIGH);

  
  Serial.begin(9600);
   
}

void loop() {
    digitalWrite(latchPin, LOW);
    int a=B00000001;
    // shift out the bits:
    Serial.println("0");
      shiftOut(dataPin, clockPin, MSBFIRST, 0); 
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
    digitalWrite(latchPin,HIGH);
      delay(5000);
    digitalWrite(latchPin,LOW);
    
    for (c=1;c<9;c++){
      
 
      shiftOut(dataPin, clockPin, MSBFIRST, b);  
      shiftOut(dataPin, clockPin, MSBFIRST, a);  
      a= a<<1;
      Serial.println(a);

      digitalWrite(latchPin, HIGH);
      // pause before next value:
      delay(10000);
      digitalWrite(latchPin,LOW);
    }
}

