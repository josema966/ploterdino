/*************************
*
* Uso del l293d para control de un motor de continua
*
*************************** */
/*
Adafruit Arduino - Lesson 15. Bi-directional Motor
*/
 
int enablePin = 11;
int in1Pin = 10;
int in2Pin = 9;
int switchPin = 7;
//int potPin = 0;



void setup()
{
  pinMode(in1Pin, OUTPUT);
  pinMode(in2Pin, OUTPUT);
  pinMode(enablePin, OUTPUT);
  pinMode(switchPin, INPUT_PULLUP);
}
 
void loop()
{
  //int speed = analogRead(potPin) / 4;
  int speedy = 255;  
  boolean reverse = digitalRead(switchPin);
  setMotor(speedy, reverse);
}
 
void setMotor(int speedy, boolean reverse)
{
  analogWrite(enablePin, speedy);
  digitalWrite(in1Pin, ! reverse);
  digitalWrite(in2Pin, reverse);
}
