/***********************
*
* Prueba del sersor paso a paso 3050 el que lee la tira
*
* Jose Maria Martinez
*********************** */
// Salen los pins 12 y 13

int leda = 12;
int ledb = 13;
int pina = 10;
int pinb = 11;


void setup(){
  
  pinMode(leda,OUTPUT);
  pinMode(ledb,OUTPUT);

  // Leo por los pines 10 y 11

  pinMode(pina,INPUT);
  pinMode(pinb,INPUT);
}

void loop(){
  if (digitalRead(pina)){
    digitalWrite(leda,HIGH);
  } else { digitalWrite(leda,LOW);}
  
  if (digitalRead(pinb)){
    digitalWrite(ledb,HIGH);
  } else { digitalWrite(ledb,LOW);}
}
  

